- name: show current user (debugging)
  debug:
    msg: "current user: {{ lookup('ansible.builtin.env', 'USER') }}"
  changed_when: false

# don't make any assumptions in case it is a new runner
- name: ensure a local .ssh directory 
  ansible.builtin.file:
    path: "{{ lookup('ansible.builtin.env', 'HOME') }}/.ssh"
    state: directory
    owner: "{{ lookup('ansible.builtin.env', 'USER') }}"
    group: "{{ lookup('ansible.builtin.env', 'USER') }}"
    mode: 0700
  delegate_to: "{{ var_controlhost }}" 

- name: check for existing ssh key
  ansible.builtin.stat:
    path: "{{ lookup('ansible.builtin.env', 'HOME') }}/.ssh/known_hosts"
  register: known_hosts
  delegate_to: "{{ var_controlhost }}" 
  changed_when: false

- name: ensure a local known_hosts file
  ansible.builtin.file:
    path: "{{ lookup('ansible.builtin.env', 'HOME') }}/.ssh/known_hosts"
    state: touch
    owner: "{{ lookup('ansible.builtin.env', 'USER') }}"
    group: "{{ lookup('ansible.builtin.env', 'USER') }}"
    mode: 0600
  delegate_to: "{{ var_controlhost }}" 
  when: known_hosts.stat.exists == false
    
- name: check for existing ssh key
  ansible.builtin.stat:
    path: "{{ lookup('ansible.builtin.env', 'HOME') }}/.ssh/{{ var_vm_name }}"
  register: ssh_key
  delegate_to: "{{ var_controlhost }}" 
  changed_when: false

- name: generate a ssh key for the default user on the host (so long as no pre-existing one)
  community.crypto.openssh_keypair:
    path: "{{ lookup('ansible.builtin.env', 'HOME') }}/.ssh/{{ var_vm_name }}"
    type: ed25519
    comment: "{{ var_vm_name }}--{{ lookup('ansible.builtin.env', 'USER') }}"
  when: var_generate_key == true and ssh_key.stat.exists == false
  delegate_to: "{{ var_controlhost }}" 

- name: read existing pub ssh key
  shell: "cat {{ lookup('ansible.builtin.env', 'HOME') }}/.ssh/{{ var_vm_name }}.pub"
  register: pub_key
  when: var_generate_key == true or ssh_key.stat.exists == true
  delegate_to: "{{ var_controlhost }}" 
  changed_when: false
 
- name: set ssh key if created or pre-existing
  ansible.builtin.set_fact: 
    var_ssh_key: "{{ pub_key.stdout }}"
  when: var_generate_key == true or ssh_key.stat.exists == true
  delegate_to: "{{ var_controlhost }}" 

#this should show the key that was supplied, created or previously created
- name: show public key for host 
  debug:
    msg: "{{ var_ssh_key }}"
  when: var_ssh_key != ""
  changed_when: false

- name: ensure there are credentials before proceeding, else fail
  ansible.builtin.fail:
    msg: "No new ssh key was requested, no pre-existing one was found, no ssh key was supplied, no password hash was supplied.  Cannot proceed with VM creation." 
  when: var_ssh_key == "" and var_password_hash == ""
