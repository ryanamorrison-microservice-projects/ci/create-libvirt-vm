create-libvirt-vm
=========

Create a custom VM for testing using libvirt.  This role was tested more extensively with Ubuntu 22 images.  There was some limited testing with Fedora 38 (cloud-init networking needs some work).

Requirements
------------
Requires virsh on control host.  This role assumes the localhost is the control host and where libvirt is installed (i.e., a CI runner).  It also assumes bridge networking to an interface (the default setting for this below is br0) instead of the virtualized bridge networking that is the default. 

This role does _not_ use the ansible module for libvirt (community.libvirt.virt) and therefore does not require the python dependency.  Most examples for the module seem to be a wrapper for virsh and use XML.  After some experimentation, it was deemed the community module didn't add value and that virsh was sufficient.       

As this role targets a CI runner use case, it has been designed to use as few privileges as possible.  The account running the role _does_ need to be a member of the libvirt unix group (e.g., `libvirt-qemu` on ubuntu).       

This roles uses [hostctl](https://github.com/guumaster/hostctl) to manage host entries and so the user account needs limited sudo access to do so.     

When creating disks in disk pools, libvirt creates the backing files as `root:root` by default which cuts off a "non-sudo" creator from being able to use them.  There does not seem to be a way to change this behavior so the user account needs to be able to reset ownership on files in the images directory (e.g., `/var/lib/libvirt/images` on ubuntu).  Disk flexibility and permissions are the primary reasons this role is not Terraform code or that Molecule + Vagrant were not used as a shortcut to manage libvirt.

Role Variables
--------------
The variable `var_vm_name` needs to be supplied via a playbook.  The following are from `defaults/main.yml`.

These should only need to be adjust if they differ on the host system (i.e., runner or hypervisor)
```
var_group: libvirt-qemu
var_image_dir: /var/lib/libvirt/images
var_template_dir: /var/lib/libvirt/templates
var_log_dir: /var/log/libvirt/qemu/
```
The user name of the account to be created on the system, the default is the account executing the playbook.
```
var_username: "{{ lookup('ansible.builtin.env', 'USER') }}"
```
The admin accoun group to be given sudoer permissions on the created VM.
```
var_admin_group: service-admins
```
Whether to generate a ssh key for the default user on the VM being created.  Note: one can be supplied instead using the next variable below.
```
var_generate_key: true
```
A supplied, quoted ssh key (use single ' quotes), will be ignored if `var_generate_key` is set to true (no default value).
```
var_ssh_key:
```
A hashed password (e.g., `openssl passwd -6 -salt $(openssl rand -base64 16)` ), no default value.
```
var_password_hash:
```
The cloud-init image to be downloaded.
```
var_image_url: "https://cloud-images.ubuntu.com/jammy/current/jammy-server-cloudimg-amd64.img"
```
libvirt-compatible os variant name, (using `osinfo-query os` requires `sudo apt install libosinfo-bin`)
```
var_os: ubuntu22.04
```
Harware settings, memory is in MB.
```
var_mem: 2048
var_cpu: 2
```
If the following two settings are not supplied, DHCP is assumed.  IPv4 address and CIDR subnet for default network adapter (e.g., 192.168.1.20/24).
```
var_ipv4:
```
Gateway no CIDR (e.g., 192.168.1.1)
```
var_gw4:
```
DNS servers, the supplied defaults are Cloudflare and Google.
```
var_dns1: 1.1.1.1
var_dns2: 8.8.8.8
```
Settings for root disk, size in GB.  The name of the root disk should be left as-is.  If it is changed, be sure to change this in the [cleanup role](https://gitlab.com/ryanamorrison-microservice-projects/ci/cleanup-libvirt-vm).
```
var_disk_size_in_gb: 16
var_root_disk_name: root-disk.qcow2
```
In some cases, this might be useful but generally shouldn't be necessary.
```
var_force_vm_creation: false
```
The default for additional disks is none.
```
var_additional_disks: {}
```
However, if more are needed they can be specified as in the following example:
```
var_additional_disks:
  vdb:
    size_in_gb: 10
  vdc:
    size_in_gb: 15
```
The existing bridge that the guest should be connected to.
```
var_net_bridge: br0
```

Dependencies
------------

No dependencies, a [companion role](https://gitlab.com/ryanamorrison-microservice-projects/ci/cleanup-libvirt-vm) was created to clean up the artifacts created by this role.

Example Playbook
----------------
The playbook should turn off default fact-gathering (as the VM or VM's has/have not yet been created).
```
---
- hosts: all
  gather_facts: false
  roles:
    - create-libvirt-vm
```
Also see `tests/` folder and `.gitlab-ci` file.   

License
-------

BSD

Author Information
------------------

Ryan A. Morrison (ryan@ryanamorrison.com)
